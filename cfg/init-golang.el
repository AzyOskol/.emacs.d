(require-package 'go-mode)
(require-package 'go-autocomplete)
(require-package 'go-direx)
(require-package 'go-errcheck)


(if (system-is-linux)
    (progn
        (setenv "GOPATH" ""))
    (progn
        (setenv "GOPATH" "C:\\GO\\")))


(provide 'go-mode-autoloads)
(provide 'auto-complete-config)

(ac-config-default)

(setq display-buffer-function 'popwin:display-buffer)
(push '("^\*go-direx:" :regexp t :position right :width 0.2 :dedicated t)
      popwin:special-display-config)


(defun keybind-go-mode-hook ()
    (add-hook 'before-save-hook 'gofmt-before-save)
    (local-set-key (kbd "<f12>") 'godef-jump))



(add-hook 'go-mode-hook 'keybind-go-mode-hook)

(provide 'init-golang)
