(defvar azy-keys-minor-mode-map (make-keymap) "azy-keys-minor-mode keymap.")


(define-minor-mode azy-keys-minor-mode
  "A minor mode so that my key settings override annoying major modes."
  t " my-keys" 'azy-keys-minor-mode-map)

;; Execute Commands
(define-key azy-keys-minor-mode-map (kbd "M-a") 'execute-extended-command)
(define-key azy-keys-minor-mode-map (kbd "M-A") 'shell-command)
;;work with text
;;select
(define-key azy-keys-minor-mode-map (kbd "C-a") 'mark-whole-buffer)
(define-key azy-keys-minor-mode-map (kbd "M-6") 'imd-select-current-block)
(define-key azy-keys-minor-mode-map (kbd "M-7") 'imd-select-current-line)


;;replace
(define-key azy-keys-minor-mode-map (kbd "M-5") 'query-replace)
(define-key azy-keys-minor-mode-map (kbd "M-%") 'query-replace-regexp)


;;delete
(define-key azy-keys-minor-mode-map (kbd "M-w") 'imd-shrink-whitespaces)

(define-key azy-keys-minor-mode-map (kbd "M-d") 'delete-backward-char)
(define-key azy-keys-minor-mode-map (kbd "M-f") 'delete-char)

(define-key azy-keys-minor-mode-map (kbd "M-e") 'backward-kill-word)
(define-key azy-keys-minor-mode-map (kbd "M-r") 'kill-word)

;;copy,paste,cut
(define-key azy-keys-minor-mode-map (kbd "C-v") 'yank)
(define-key azy-keys-minor-mode-map (kbd "C-c") 'kill-ring-save)
(define-key azy-keys-minor-mode-map (kbd "C-x") 'kill-region)
;;end wwt

;;navigation
(define-key azy-keys-minor-mode-map (kbd "C-g") 'goto-line)

(define-key azy-keys-minor-mode-map (kbd "M-i") 'previous-line)
(define-key azy-keys-minor-mode-map (kbd "M-j") 'backward-char)
(define-key azy-keys-minor-mode-map (kbd "M-k") 'next-line)
(define-key azy-keys-minor-mode-map (kbd "M-l") 'forward-char)

(define-key azy-keys-minor-mode-map (kbd "M-I") 'scroll-down)
(define-key azy-keys-minor-mode-map (kbd "M-K") 'scroll-up)
(define-key azy-keys-minor-mode-map (kbd "M-J") 'imd-backward-open-bracket)
(define-key azy-keys-minor-mode-map (kbd "M-L") 'imd-forward-close-bracket)

(define-key azy-keys-minor-mode-map (kbd "M-u") 'backward-word)
(define-key azy-keys-minor-mode-map (kbd "M-o") 'forward-word)
(define-key azy-keys-minor-mode-map (kbd "M-U") 'backward-paragraph)
(define-key azy-keys-minor-mode-map (kbd "M-O") 'forward-paragraph)

;;buffer
(define-key azy-keys-minor-mode-map (kbd "C-n") 'imd-new-empty-buffer)
(define-key azy-keys-minor-mode-map (kbd "C-S-t") 'imd-reopen-killed-file)
(define-key azy-keys-minor-mode-map (kbd "C-w") 'kill-this-buffer)
(define-key azy-keys-minor-mode-map (kbd "C-S-<tab>") 'previous-buffer)
(define-key azy-keys-minor-mode-map (kbd "C-<tab>") 'next-buffer)

;;file
(define-key azy-keys-minor-mode-map (kbd "C-o") 'find-file)
(define-key azy-keys-minor-mode-map (kbd "C-s") 'save-buffer)
(define-key azy-keys-minor-mode-map (kbd "C-S-s") 'write-file)

;;search
(define-key azy-keys-minor-mode-map (kbd "C-f") 'isearch-forward)
(define-key azy-keys-minor-mode-map (kbd "C-S-f") 'isearch-backward)
(define-key azy-keys-minor-mode-map (kbd "<f3>") 'isearch-repeat-forward)
(define-key azy-keys-minor-mode-map (kbd "M-<f3>") 'isearch-repeat-backward)
(define-key isearch-mode-map [escape] 'isearch-abort)
;;(define-key azy-keys-minor-mode-map (kbd "<esc> g") 'imd-goog)
;;(define-key azy-keys-minor-mode-map (kbd "<esc> j s") 'imd-jsref)

;;undo,redo
(define-key azy-keys-minor-mode-map (kbd "C-z") 'undo)
(define-key azy-keys-minor-mode-map (kbd "C-S-z") 'redo)

;;window
(define-key azy-keys-minor-mode-map (kbd "M-3") 'delete-other-windows)
(define-key azy-keys-minor-mode-map (kbd "M-$") 'split-window-below)
(define-key azy-keys-minor-mode-map (kbd "M-4") 'split-window-right)
(define-key azy-keys-minor-mode-map (kbd "M-2") 'delete-window)


(azy-keys-minor-mode 1)
(provide 'init-global-keys)
