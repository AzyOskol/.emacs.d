;;(require-package 'smart-mode-line)
;;(require-package 'smart-mode-line-powerline-theme)
(require-package 'powerline)

(setq powerline-color1 "#073642")
(setq powerline-color2 "#002b36")

(set-face-attribute 'mode-line nil
                    :foreground "#fdf6e3"
                    :background "#2aa198"
                    :box nil)
(set-face-attribute 'mode-line-inactive nil
                    :box nil)
(setq powerline-default-separator 'butt)


(powerline-default-theme)

(provide 'init-powerline)
