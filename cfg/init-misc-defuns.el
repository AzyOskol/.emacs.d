(defun imd-replace-last-sexp ()
    (interactive)
    (let ((value (eval (preceding-sexp))))
        (kill-sexp -1)
        (insert (format "%s" value))))

(defun imd-undosify ()
    (interactive)
    (goto-char (point-min))
    (while (search-forward "\r" nil t) (replace-match "")))

(defun imd-global-text-scale-increase ()
    (interactive)
    (set-face-attribute 'default nil :height (+ (face-attribute 'default :height) 20)))

(defun imd-global-text-scale-decrease ()
    (interactive)
    (set-face-attribute 'default nil :height (- (face-attribute 'default :height) 20)))

(defun imd-global-text-scale-reset ()
    (interactive)
    (set-face-attribute 'default nil :height 150))
(imd-global-text-scale-reset)

(defun imd-goog(term)
    (interactive "sGoogle search: ")
    (browse-url
     (concat "http://www.google.com/search?q="
             (replace-regexp-in-string " " "+" term))))

(defun imd-jsref(term)
    (interactive "sJavascript search: ")
    (browse-url
     (concat "http://www.google.com/search?btnI=I%27m+Feeling+Lucky&q=site%3Adeveloper.mozilla.org/en-US/docs/Web/JavaScript/Reference+"
             (replace-regexp-in-string " " "+" term))))

(defun imd-copy-whole-buffer()
    (interactive)
    (mark-whole-buffer)
    (copy-region-as-kill (region-beginning) (region-end)))

;;buffer

(defvar initial-major-mode)
(defvar buffer-offer-save)

(defun imd-new-empty-buffer ()
    "Opens a new empty buffer."
    (interactive)
    (let ((buf (generate-new-buffer "untitled")))
        (switch-to-buffer buf)
        (funcall (and initial-major-mode))
        (setq buffer-offer-save t)))

(defvar killed-file-list nil
  "List of recently killed files.")

(defun add-file-to-killed-file-list ()
    "If buffer is associated with a file name, add that file to the
`killed-file-list' when killing the buffer."
    (when buffer-file-name
        (push buffer-file-name killed-file-list)))

(add-hook 'kill-buffer-hook #'add-file-to-killed-file-list)

(defun imd-reopen-killed-file ()
    "Reopen the most recently killed file, if one exists."
    (interactive)
    (when killed-file-list
        (find-file (pop killed-file-list))))

;;; CURSOR MOVEMENT
(defun imd-forward-open-bracket (&optional number)
    "Move cursor to the next occurrence of left bracket or quotation mark.
With prefix NUMBER, move forward to the next NUMBER left bracket or quotation mark.
With a negative prefix NUMBER, move backward to the previous NUMBER left bracket or quotation mark."
    (interactive "p")
    (if (and number
             (> 0 number))
        (imd-backward-open-bracket (- 0 number))
        (forward-char 1)
        (search-forward-regexp
         (eval-when-compile
             (regexp-opt
              '("\"" "(" "{" "[" "<" "'"))) nil t number)
        (backward-char 1)))

(defun imd-backward-open-bracket (&optional number)
    "Move cursor to the previous occurrence of left bracket or quotation mark.
With prefix argument NUMBER, move backward NUMBER open brackets.
With a negative prefix NUMBER, move forward NUMBER open brackets."
    (interactive "p")
    (if (and number
             (> 0 number))
        (imd-forward-open-bracket (- 0 number))
        (search-backward-regexp
         (eval-when-compile
             (regexp-opt
              '("\"" "(" "{" "[" "<" "'"))) nil t number)))

(defun imd-forward-close-bracket (&optional number)
    "Move cursor to the next occurrence of right bracket or quotation mark.
With a prefix argument NUMBER, move forward NUMBER closed bracket.
With a negative prefix argument NUMBER, move backward NUMBER closed brackets."
    (interactive "p")
    (if (and number
             (> 0 number))
        (imd-backward-close-bracket (- 0 number))
        (search-forward-regexp
         (eval-when-compile
             (regexp-opt '("\"" ")" "]" "}" ">" "'"))) nil t number)))

(defun imd-backward-close-bracket (&optional number)
    "Move cursor to the previous occurrence of right bracket or quotation mark.
With a prefix argument NUMBER, move backward NUMBER closed brackets.
With a negative prefix argument NUMBER, move forward NUMBER closed brackets."
    (interactive "p")
    (if (and number
             (> 0 number))
        (imd-forward-close-bracket (- 0 number))
        (backward-char 1)
        (search-backward-regexp
         (eval-when-compile
             (regexp-opt '("\"" ")" "]" "}" ">" "'"))) nil t number)
        (forward-char 1)))

(defun imd-shrink-whitespaces ()
    "Remove white spaces around cursor to just one or none.
If current line does have visible chars, then shrink whitespace surrounding cursor to just one space.
If current line does not have visible chars, then shrink al neighboring blank lines to just one.
If current line is a single space, remove that space.
Calling this command 3 times will always result in no whitespaces around cursor."
    (interactive)
    (let (cursor-point
          line-has-meat-p  ; current line contains non-white space chars
          space-tab-neighbor-p
          space-or-tab-begin space-or-tab-end
          line-begin-pos line-end-pos)
        (save-excursion
            ;; todo: might consider whitespace as defined by syntax table, and also consider whitespace chars in unicode if syntax table doesn't already considered it.
            (setq cursor-point (point))

            (setq space-tab-neighbor-p (if (or (looking-at " \\|\t") (looking-back " \\|\t")) t nil) )
            (move-beginning-of-line 1) (setq line-begin-pos (point) )
            (move-end-of-line 1) (setq line-end-pos (point) )
            ;;       (re-search-backward "\n$") (setq line-begin-pos (point) )
            ;;       (re-search-forward "\n$") (setq line-end-pos (point) )
            (setq line-has-meat-p (if (< 0 (count-matches "[[:graph:]]" line-begin-pos line-end-pos)) t nil) )
            (goto-char cursor-point)

            (skip-chars-backward "\t ")
            (setq space-or-tab-begin (point))

            (skip-chars-backward "\t \n")

            (goto-char cursor-point)
            (skip-chars-forward "\t ")
            (setq space-or-tab-end (point))
            (skip-chars-forward "\t \n")
            )

        (if line-has-meat-p
            (let (deleted-text)
                (when space-tab-neighbor-p
                    ;; remove all whitespaces in the range
                    (setq deleted-text (delete-and-extract-region space-or-tab-begin space-or-tab-end))
                    ;; insert a whitespace only if we have removed something
                    ;; different that a simple whitespace
                    (if (not (string= deleted-text " "))
                        (insert ""))))

            (progn
                (delete-blank-lines))
            ;; todo: possibly code my own delete-blank-lines here for better efficiency, because delete-blank-lines seems complex.
            )))

(defun imd-select-current-block ()
    "Select the current block of next between empty lines."
    (interactive)
    (let (p1)
        (if (re-search-backward "\n[ \t]*\n" nil "move")
            (progn (re-search-forward "\n[ \t]*\n")
                   (setq p1 (point)))
            (setq p1 (point)))
        (if (re-search-forward "\n[ \t]*\n" nil "move")
            (re-search-backward "\n[ \t]*\n"))
        (set-mark p1)))

(defun imd-select-current-line ()
    "Select the current line"
    (interactive)
    (end-of-line) ; move to end of line
    (set-mark (line-beginning-position)))


(defun imd-select-text-in-quote ()
    "Select text between the nearest left and right delimiters.
Delimiters are paired characters:
(\" () {} [] <> '
    For practical purposes, also: \"\", but not single quotes."
    (interactive)
    (let (p1)
        (skip-chars-backward "^<>\"({[<'")
        (setq p1 (point))
        (skip-chars-forward "^<>\")}]>'")
        (set-mark p1)))

;; by Nikolaj Schumacher, 2008-10-20. Released under GPL.
(defun imd-semnav-up (arg)
    (interactive "p")
    (when (nth 3 (syntax-ppss))
        (if (> arg 0)
            (progn
                (skip-syntax-forward "^\"")
                (goto-char (1+ (point)))
                (setq arg (1- arg) ))
            (skip-syntax-backward "^\"")
            (goto-char (1- (point)))
            (setq arg (1+ arg) )))
    (up-list arg))

;; by Nikolaj Schumacher, 2008-10-20. Released under GPL.
(defun imd-extend-selection (arg &optional incremental)
    "Select the current word.
Subsequent calls expands the selection to larger semantic unit."
    (interactive (list (prefix-numeric-value current-prefix-arg)
                       (or (and transient-mark-mode mark-active)
                           (eq last-command this-command))))
    (if incremental
        (progn
            (imd-semnav-up (- arg))
            (forward-sexp)
            (mark-sexp -1))
        (if (> arg 1)
            (imd-extend-selection (1- arg) t)
            (if (looking-at "\\=\\(\\s_\\|\\sw\\)*\\_>")
                (goto-char (match-end 0))
                (unless (memq (char-before) '(?\) ?\"))
                    (forward-sexp)))
            (mark-sexp -1))))


(provide 'init-misc-defuns)
