;; Bootstrap
(add-to-list 'load-path (expand-file-name "cfg/" user-emacs-directory))
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(require 'init-general)
(require 'init-packages)
(require 'init-exec-path)
(require 'init-popwin)
(require 'init-direx)

;; Visuals
(require 'init-ui)
(require 'init-theme)

;; Tools
;;(require 'init-ace)
(require 'init-desktop)
;;(require 'init-git)
;;(require 'init-helm)
;;(require 'init-org)
;;(require 'init-projectile)
(require 'init-tramp)

;; Coding
(require 'init-auto-complite)
(require 'init-edit-setup)
(require 'init-prog-setup)
;;(require 'init-elisp)
;;(require 'init-xml)
;;(require 'init-yaml)
(require 'init-golang)
;; Personalization
;;(require 'init-ergoemacs-mode)
(require 'init-powerline)
(require 'init-redo+)
(require 'init-misc-defuns)
(require 'init-global-keys)
(load custom-file)
